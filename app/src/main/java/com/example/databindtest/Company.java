package com.example.databindtest;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.BindingAdapter;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by MrBoolean on 2017/3/5.
 */

public class Company extends BaseObservable {
    public String name;
    private String icon;
    private String info;
    private long createTime;

    public Company(String name, String icon, String info, long createTime) {
        this.name = name;
        this.icon = icon;
        this.info = info;
        this.createTime = createTime;
    }

    @Bindable
    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    @BindingAdapter({"bind:imageUrl", "bind:error"})
    public static void updateIcon(ImageView view, String url, Drawable error) {
        Glide.with(view.getContext()).load(url).error(error).into(view);
    }

    @Bindable
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Bindable
    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    @Bindable
    public long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }


    public static List<Company> getCompanyList() {
        List<Company> companies = new ArrayList<>();
        Calendar calendar = Calendar.getInstance();

        for (int i = 0; i < 50; i++) {
            String name = "Baidu" + i;
            String img = "https://ss0.baidu.com/6ONWsjip0QIZ8tyhnq/it/u=830279856,2339290172&fm=58";
            if (i % 3 == 1) {
                img = "https://gss0.baidu.com/-Po3dSag_xI4khGko9WTAnF6hhy/zhidao/pic/item/3b87e950352ac65c1b6a0042f9f2b21193138a97.jpg";
            } else if (i % 3 == 2) {
                img = "https://gss0.baidu.com/-vo3dSag_xI4khGko9WTAnF6hhy/zhidao/pic/item/738b4710b912c8fcb60691fcfe039245d688210d.jpg";
            }
            companies.add(new Company(name, img, "全球最大的中文搜索引擎、最大的中文网站。" +
                    "1999年底,身在美国硅谷的李彦宏看到了中国互联网及中文搜索引擎服务的巨大发展潜力，" +
                    "抱着技术改变世界的梦想，他毅然辞掉硅谷的高薪工作，" +
                    "携搜索引擎专利技术，于 2000年... ", calendar.getTime().getTime()));
            calendar.set(Calendar.DAY_OF_YEAR, calendar.get(Calendar.DAY_OF_YEAR) + 1);
        }
        return companies;
    }

    @Override
    public String toString() {
        return
                "name=" + name + "\n\n" +
                        "icon=" + icon + "\n\n" +
                        "info=" + info + "\n\n" +
                        "createTime=" + DateUtil.getTime(createTime);
    }
}
