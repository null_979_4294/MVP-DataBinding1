package com.example.databindtest;

import android.databinding.Bindable;
import android.databinding.BindingAdapter;
import android.databinding.InverseBindingAdapter;
import android.databinding.InverseBindingMethod;
import android.databinding.InverseBindingMethods;
import android.databinding.InverseMethod;
import android.databinding.ObservableField;
import android.databinding.ObservableLong;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by MrBoolean on 2017/3/5.
 */

public class BRCompany extends BRBaseObservable{
    public final ObservableField<String> nameField = new ObservableField<>();
    public final ObservableField<String> iconField = new ObservableField<>();
    public final ObservableField<String> infoField = new ObservableField<>();
    public final ObservableLong createTimeField = new ObservableLong();

    public BRCompany(String name, String icon, String info, long createTime) {
        this.nameField.set(name);
        this.iconField.set(icon);
        this.infoField.set(info);
        this.createTimeField.set(createTime);
    }

    @Bindable
    public String getBRIcon() {
        return iconField.get();
    }

    public void setIcon(String icon) {
        this.iconField.set(icon);
    }

    @BindingAdapter({"bind:imageUrl", "bind:error"})
    public static void setIcon(ImageView view, String url, Drawable error) {
        Glide.with(view.getContext()).load(url).error(error).into(view);
    }

    @Bindable
    public String getBRName() {
        return nameField.get();
    }

    public void setName(String name) {
        this.nameField.set(name);
    }

    @Bindable
    public String getBRInfo() {
        return infoField.get();
    }

    public void setInfo(String info) {
        this.infoField.set(info);
    }

    @Bindable
    public long getBRCreateTime() {
        return createTimeField.get();
    }

    public void setCreateTime(long createTime) {
        this.createTimeField.set(createTime);
    }


    public static List<BRCompany> getCompanyList() {
        List<BRCompany> companies = new ArrayList<>();
        Calendar calendar = Calendar.getInstance();

        for(int i=0; i<50; i++) {
            String name = "Baidu"+i;
            String img = "https://ss0.baidu.com/6ONWsjip0QIZ8tyhnq/it/u=830279856,2339290172&fm=58";
            if(i % 3 == 1) {
                img = "https://gss0.baidu.com/-Po3dSag_xI4khGko9WTAnF6hhy/zhidao/pic/item/3b87e950352ac65c1b6a0042f9f2b21193138a97.jpg";
            }else if(i % 3 == 2) {
                img = "https://gss0.baidu.com/-vo3dSag_xI4khGko9WTAnF6hhy/zhidao/pic/item/738b4710b912c8fcb60691fcfe039245d688210d.jpg";
            }
            companies.add(new BRCompany(name,img,"全球最大的中文搜索引擎、最大的中文网站。" +
                    "1999年底,身在美国硅谷的李彦宏看到了中国互联网及中文搜索引擎服务的巨大发展潜力，" +
                    "抱着技术改变世界的梦想，他毅然辞掉硅谷的高薪工作，" +
                    "携搜索引擎专利技术，于 2000年... ",calendar.getTime().getTime()));
            calendar.set(Calendar.DAY_OF_YEAR,calendar.get(Calendar.DAY_OF_YEAR)+1);
        }
        return companies;
    }

    @Override
    public String toString() {
        return
                "name=" + nameField.get() + "\n\n" +
                "icon=" + iconField.get() + "\n\n" +
                "info=" + infoField.get() + "\n\n" +
                "createTime=" + DateUtil.getTime(createTimeField.get());
    }
}
