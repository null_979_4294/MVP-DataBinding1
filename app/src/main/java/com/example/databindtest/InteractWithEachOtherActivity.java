package com.example.databindtest;

import android.databinding.DataBindingUtil;
import android.databinding.Observable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.example.databindtest.databinding.BrCompanyItemLayoutBinding;

/**
 * Created by MrBoolean on 2017/3/18.
 */

public class InteractWithEachOtherActivity extends AppCompatActivity {

    private BRCompany mCompany;
    private BrCompanyItemLayoutBinding mBinding;
    private int mCount = 0;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.interact_with_each_other_activity_layout);
        mBinding = DataBindingUtil.bind(findViewById(R.id.br_company_item_root_layout));
        mCompany = BRCompany.getCompanyList().get(0);
        mBinding.setActionHandler(new ActionHandler(this));
        mBinding.setBrCompany(mCompany);
        initPropertyChanged();
    }

    public void updateData(View view) {
        mCount++;
        mBinding.tvInfo.setText(mCount+""+mBinding.tvInfo.getText());
        mBinding.tvName.setText(mCount+""+mBinding.tvName.getText());
    }


    private void initPropertyChanged() {
        if(mCompany == null) {
            return;
        }

        mCompany.infoField.addOnPropertyChangedCallback(new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable observable, int i) {
                Log.d("onPropertyChanged","info:"+mCompany.infoField.get());
            }
        });

        mCompany.nameField.addOnPropertyChangedCallback(new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable observable, int i) {
                Log.d("onPropertyChanged","name:"+mCompany.nameField.get());
            }
        });
    }

}
