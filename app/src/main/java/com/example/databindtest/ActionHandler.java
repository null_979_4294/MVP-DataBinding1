package com.example.databindtest;

import android.content.Context;
import android.media.Image;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import uk.co.senab.photoview.PhotoView;

/**
 * Created by MrBoolean on 2017/3/6.
 */

public class ActionHandler {
    private Context mContext;

    public ActionHandler(Context context) {
        this.mContext = context;
    }

    public void showTextDialog(String text) {
        new AlertDialog.Builder(mContext).setTitle("detail")
                .setMessage(text).create().show();
    }

    public void showImageDialog(String imagePath) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.image_dialog_layout,null);
        new AlertDialog.Builder(mContext).setView(view).create().show();
        Glide.with(mContext).load(imagePath).into((ImageView) view.findViewById(R.id.iv_photo));
    }
}
