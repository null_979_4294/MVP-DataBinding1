package com.example.databindtest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

public class BaseActivity extends AppCompatActivity {

    protected void addTextChangeListener(EditText editText, final PropertyChangedActivity.Action1<String> action1) {
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(action1 != null) {
                    action1.call(s.toString());
                }
            }
        });
    }
}
