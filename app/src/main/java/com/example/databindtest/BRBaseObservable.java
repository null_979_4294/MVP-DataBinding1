package com.example.databindtest;

import android.databinding.BaseObservable;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by MrBoolean on 2017/3/15.
 */

public class BRBaseObservable extends BaseObservable{
    protected HashMap<BaseObservable,List<OnPropertyChangedCallback>> mOnPropertyChangedCallbackMap;

    public void addOnPropertyChangedCallback(BaseObservable baseObservable,OnPropertyChangedCallback onPropertyChangedCallback) {
        if(baseObservable==null || onPropertyChangedCallback==null) {
            return;
        }
        if(mOnPropertyChangedCallbackMap == null) {
            mOnPropertyChangedCallbackMap = new HashMap<>();
        }
        List<OnPropertyChangedCallback> changedCallbacks = mOnPropertyChangedCallbackMap.get(baseObservable);
        if(changedCallbacks==null) {
            changedCallbacks = new LinkedList<>();
            mOnPropertyChangedCallbackMap.put(baseObservable,changedCallbacks);
        }
        if(!changedCallbacks.contains(onPropertyChangedCallback)) {
            changedCallbacks.add(onPropertyChangedCallback);
        }
        baseObservable.addOnPropertyChangedCallback(onPropertyChangedCallback);
    }

    public void dispose() {
        if(mOnPropertyChangedCallbackMap == null) {
            return;
        }
        Set<Map.Entry<BaseObservable,List<OnPropertyChangedCallback>>> entrySet = mOnPropertyChangedCallbackMap.entrySet();
        Iterator<Map.Entry<BaseObservable,List<OnPropertyChangedCallback>>> iterator = entrySet.iterator();
        while (iterator.hasNext()) {
            Map.Entry<BaseObservable,List<OnPropertyChangedCallback>> entry = iterator.next();
            BaseObservable baseObservable = entry.getKey();
            List<OnPropertyChangedCallback> list = entry.getValue();
            if(list!=null && baseObservable!=null) {
                for(OnPropertyChangedCallback changedCallback : list) {
                    baseObservable.removeOnPropertyChangedCallback(changedCallback);
                }
            }
        }
    }
}
