package com.example.databindtest;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;


import com.example.databindtest.databinding.CompanyItemLayoutBinding;

import java.util.List;

public class MainActivity extends BaseActivity {
    private Company mCompany;
    CompanyItemLayoutBinding mBinding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mBinding = DataBindingUtil.bind(findViewById(R.id.company_item_root_layout));
        mCompany = Company.getCompanyList().get(0);
        mBinding.setActionHandler(new ActionHandler(this));
        mBinding.setCompany(mCompany);
        initView();
    }

    private void initView() {
        ((EditText)findViewById(R.id.et_company_icon_path)).setText(mCompany.getIcon());
        ((EditText)findViewById(R.id.et_company_name)).setText(mCompany.getName());
        ((EditText)findViewById(R.id.et_company_info)).setText(mCompany.getInfo());
    }

    public void setting(View view) {
        mCompany.setIcon(((EditText)findViewById(R.id.et_company_icon_path)).getText().toString());
        mCompany.setName(((EditText)findViewById(R.id.et_company_name)).getText().toString());
        mCompany.setInfo(((EditText)findViewById(R.id.et_company_info)).getText().toString());
        mBinding.setCompany(mCompany);
    }

    public void testListView(View view) {
        DataBindingDialogFragment.getInstance().show(getSupportFragmentManager(),"test");
    }

    public void testPropertyChange(View view) {
        Intent intent = new Intent();
        intent.setClass(this,PropertyChangedActivity.class);
        startActivity(intent);
    }

    public void tesInteract(View view) {
        Intent intent = new Intent();
        intent.setClass(this,InteractWithEachOtherActivity.class);
        startActivity(intent);
    }

    public static class DataBindingDialogFragment extends DialogFragment {
        public static DataBindingDialogFragment getInstance() {
            DataBindingDialogFragment dataBindingDialogFragment = new DataBindingDialogFragment();
            return dataBindingDialogFragment;
        }

        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            RecyclerView recyclerView = new RecyclerView(getContext());
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            recyclerView.setAdapter(new DataBindingRecyclerViewAdapter(getContext()));
            recyclerView.addItemDecoration(new DividerItemDecoration(getContext(),DividerItemDecoration.HORIZONTAL));
            return recyclerView;
        }
    }

    private static class DataBindingRecyclerViewAdapter extends RecyclerView.Adapter {
        private Context mContext;
        private List<Company> mCompanies;
        private ActionHandler mActionHandler;
        public DataBindingRecyclerViewAdapter(Context context) {
            mContext = context;
            mCompanies = Company.getCompanyList();
            mActionHandler = new ActionHandler(mContext);
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            RecyclerView.ViewHolder viewHolder = new RecyclerView.ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.company_item_layout,null)) {
            };
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            CompanyItemLayoutBinding binding = DataBindingUtil.bind(holder.itemView);
            binding.tvName.setTextSize(10);
            binding.tvCreateTime.setTextSize(10);
            binding.tvInfo.setTextSize(10);
            binding.setActionHandler(mActionHandler);
            binding.setCompany(mCompanies.get(position));
        }

        @Override
        public int getItemCount() {
            return mCompanies==null?0:mCompanies.size();
        }
    }
}
