package com.example.databindtest;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import java.text.SimpleDateFormat;

/**
 * Created by MrBoolean on 2017/3/5.
 */

public class DateUtil{

    public static String getTime(long time) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        return sdf.format(time);
    }
}
