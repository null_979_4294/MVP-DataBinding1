package com.example.databindtest;

import android.databinding.DataBindingUtil;
import android.databinding.Observable;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.widget.EditText;

import com.example.databindtest.databinding.BrCompanyItemLayoutBinding;


public class PropertyChangedActivity extends BaseActivity {

    private BRCompany mCompany;
    BrCompanyItemLayoutBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.property_change_test_activity);
        mBinding = DataBindingUtil.bind(findViewById(R.id.br_company_item_root_layout));
        mCompany = BRCompany.getCompanyList().get(1);
        mBinding.setActionHandler(new ActionHandler(this));
        mBinding.setBrCompany(mCompany);
        initView();
        initPropertyChanged();
    }

    private void initView() {
        EditText companyIconPathEditText = ((EditText)findViewById(R.id.et_company_icon_path));
        companyIconPathEditText.setText(mCompany.getBRIcon());
        EditText companyNameEditText = ((EditText)findViewById(R.id.et_company_name));
        companyNameEditText.setText(mCompany.getBRName());
        EditText companyInfoEditText =  ((EditText)findViewById(R.id.et_company_info));
        companyInfoEditText.setText(mCompany.getBRInfo());

        addTextChangeListener(companyIconPathEditText, new Action1<String>() {
            @Override
            public void call(String s) {
                mCompany.setIcon(s);
            }
        });
        addTextChangeListener(companyNameEditText, new Action1<String>() {
            @Override
            public void call(String s) {
                mCompany.setName(s);
            }
        });
        addTextChangeListener(companyInfoEditText, new Action1<String>() {
            @Override
            public void call(String s) {
                mCompany.setInfo(s);
            }
        });
    }



    private void initPropertyChanged() {
        if(mCompany == null) {
            return;
        }
        mCompany.addOnPropertyChangedCallback(mCompany.createTimeField, new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable observable, int i) {
                Log.d("","");
            }
        });
        mCompany.addOnPropertyChangedCallback(mCompany.nameField, new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable observable, int i) {
                Log.d("","");
            }
        });
        mCompany.addOnPropertyChangedCallback(mCompany.iconField, new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable observable, int i) {
                Log.d("","");
            }
        });

        mCompany.nameField.addOnPropertyChangedCallback(new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable observable, int i) {
                Log.d("onPropertyChanged","name:"+mCompany.nameField);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onDestroy() {
        mCompany.dispose();
        super.onDestroy();
    }

    public interface Action1<T>{
        void call(T t);
    }
}
